const app = getApp();
import Toast from "../../miniprogram_npm/@vant/weapp/toast/toast";
import Dialog from "../../miniprogram_npm/@vant/weapp/dialog/dialog";

Page({
  data: {
    houseId: "",
    password: "",
    openId: "",
    name: "",
  },
  // 事件处理函数

  onLoad(e) {
    let openId = wx.getStorageSync("openId");
    if (openId == "" || openId.length < 5) {
      this.getOpenId();
    }
    let houseId = wx.getStorageSync("houseId");
    let password = wx.getStorageSync("password");
    let name = wx.getStorageSync("name");

    this.setData({
      openId,
      houseId,
      password,
      name,
    });

    console.log(e);
    if (e.share == "true") {
      this.setData({
        houseId: e.houseId,
        password: e.password,
      });
      this.joinHouse();
    }
  },

  getOpenId() {
    let that = this;
    let openId;
    wx.cloud
      .callFunction({
        name: "getOpenId",
      })
      .then((res) => {
        console.log(res.result.OPENID);
        openId = res.result.OPENID;
        wx.setStorageSync("openId", openId);
        that.setData({
          openId,
        });
      });
  },

  onClickIcon() {
    Toast("没有房间会自动创建房间");
  },

  joinHouse() {
    if (
      this.data.name == "" ||
      this.data.houseId == "" ||
      this.data.password == ""
    ) {
      Toast.fail("信息不全");
      return;
    }
    if (this.data.openId == "") {
      Toast.fail("未获取到用户OpenId");
      return;
    }

    wx.setStorageSync("houseId", this.data.houseId);
    wx.setStorageSync("password", this.data.password);
    wx.setStorageSync("name", this.data.name);
    wx.setStorageSync("openId", this.data.openId);

    Toast.loading({
      message: "加入中...",
      forbidClick: true,
    });
    let that = this;
    wx.request({
      url: app.globalData.service + "/api/queryHouse",
      method: "POST",
      header: {
        "content-type": "application/x-www-form-urlencoded",
      },
      data: {
        id: that.data.houseId,
        password: that.data.password,
        openId: that.data.openId,
        name: that.data.name,
      },
      success: function (res) {
        console.log(res);
        if (res.data.err == 0) {
          let code = res.data.res;
          if (code == 0) {
            wx.redirectTo({
              url: "../../pages/pan/index",
            });
            // 房主，创建新房间
            return;
          }
          if (code == 1) {
            wx.redirectTo({
              url: "../../pages/pan/index",
            });
            // 房主，还未选片
            return;
          }
          if (code == 2) {
            wx.redirectTo({
              url: "../../pages/player/index?owner=1",
            });
            // 房主，正在播放
            return;
          }
          if (code == 3) {
            wx.redirectTo({
              url: "../../pages/player/index?owner=0",
            });
            // 用户，正在播放
            return;
          }
        } else {
          Dialog.alert({
            message: res.data.res,
          });
        }
      },
      complete: function () {
        Toast.clear();
      },
    });
  },
});
