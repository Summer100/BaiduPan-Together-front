const app = getApp()
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';

Page({
  data: {
    nowPath: '/',
    showQrCode: false,
    qrCodeUrl: '',
    openId: '',
    password: '',
    houseId: '',
    fileList: [],
    fileHistory: ['/'],
    isChange: false,
    showWatingAdd: false,
    choosed: '',
    adTime: '',
    adToken: '',
    adProgress: ''
  },
  // 事件处理函数

  onLoad(e) {
    this.data.openId = wx.getStorageSync('openId')
    this.data.password = wx.getStorageSync('password')
    this.data.houseId = wx.getStorageSync('houseId')
    this.chechLogin()

    if (e.change == 'true') {
      this.data.isChange = true
    }
  },

  chechLogin() {
    let that = this
    Toast.loading({
      message: '正在获取登录状态',
      forbidClick: true,
    });
    wx.request({
      url: app.globalData.service + '/api/isLogin',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        houseId: that.data.houseId,
        password: that.data.password
      },
      success: function (res) {
        Toast.clear()
        console.log(res)
        if (res.data.err == 0) {
          that.getFileList('/')
        } else {
          that.getQrcode()
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  getQrcode() {
    let that = this
    Toast.loading({
      message: '正在获取二维码...',
      forbidClick: true,
    });
    wx.request({
      url: app.globalData.service + '/api/getLoginCode',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        houseId: that.data.houseId,
        password: that.data.password
      },
      success: function (res) {
        console.log(res)
        if (res.data.err == 0) {
          let respData = res.data.res
          console.log(respData)
          let qrCodeUrl = respData
          that.setData({
            qrCodeUrl,
          })
          that.showQrCode()
        } else {
          Dialog.alert({
            message: res.data.res,
          })
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  checkIsLogin() {
    let that = this
    Toast.loading({
      message: '正在确认...',
      forbidClick: true,
    });
    wx.request({
      url: app.globalData.service + '/api/checkLoginStatus',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        houseId: that.data.houseId,
        password: that.data.password
      },
      success: function (res) {
        console.log(res)
        if (res.data.err == 0) {
          let respData = res.data.res
          that.hideQrCode()
          that.getFileList('/')
        } else {
          Dialog.alert({
            selector: "#van-dialog-err",
            message: res.data.res,
          })
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  showQrCode() {
    this.setData({
      showQrCode: true
    })
  },

  hideQrCode() {
    this.setData({
      showQrCode: false
    })
  },

  getFileList(path) {
    console.log(path)
    let that = this
    Toast.loading({
      message: '正在获取目录',
      forbidClick: true,
    });
    wx.request({
      url: app.globalData.service + '/api/getFileList',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        path,
        houseId: that.data.houseId,
        password: that.data.password
      },
      success: function (res) {
        // console.log(res)
        if (res.data.err == 0) {
          let respData = res.data.res
          let listTemp = JSON.parse(respData)
          // console.log(respData)
          that.data.fileHistory.push(path)
          that.setData({
            fileList: listTemp.list,
            nowPath: path
          })
          console.log(that.data.fileList)

        } else {
          Dialog.alert({
            selector: "#van-dialog-err",
            message: res.data.res,
          })
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  onClickIcon(e) {
    console.log(e)
    let isDir = e.currentTarget.dataset.isdir
    let path = e.currentTarget.dataset.path

    if (isDir) {
      this.getFileList(path)
      return
    } else {
      this.playChoosed(path) // 播放视频
    }
  },

  backTo() {
    if (this.data.fileHistory.length == 1) {
      Toast.fail("无路可退")
      return
    }
    let path = this.data.fileHistory[this.data.fileHistory.length - 2]
    console.log(path)
    let that = this
    Toast.loading({
      message: '正在获取目录',
      forbidClick: true,
    });
    wx.request({
      url: app.globalData.service + '/api/getFileList',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        path,
        houseId: that.data.houseId,
        password: that.data.password
      },
      success: function (res) {
        console.log(res)
        if (res.data.err == 0) {
          let respData = res.data.res
          let listTemp = JSON.parse(respData)
          // console.log(respData)
          that.setData({
            fileList: listTemp.list,
            nowPath: path
          })
          console.log(that.data.fileList)
          that.data.fileHistory.pop()
        } else {
          Dialog.alert({
            selector: "#van-dialog-err",
            message: res.data.res,
          })
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  playChoosed(path) {
    Toast.loading({
      message: '正准备播放',
      forbidClick: true,
      duration: 0
    });
    this.data.choosed = path
    let that = this
    wx.request({
      url: app.globalData.service + '/api/playCurrentVideo',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openId: that.data.openId,
        path,
        houseId: that.data.houseId,
        password: that.data.password,
        adToken: that.data.adToken
      },
      success: function (res) {
        console.log(res)
        if (res.data.err == 0) {
          let respData = res.data.res
          console.log(respData)
          wx.redirectTo({
            url: '../../pages/player/index?owner=1&change=' + (that.data.isChange ? 'true' : 'false'),
          })
        } else {
          let respData = res.data.res
          let errno = respData.errno
          if (errno) {
            if (errno == 133) {
              that.waitingAdd(respData.ltime, respData.adToken)
            }
          } else {
            Dialog.alert({
              selector: "#van-dialog-err",
              message: res.data.res,
            })
          }
        }
      },
      complete: function () {
        Toast.clear()
      }
    })
  },

  waitingAdd(time, token) {
    this.setData({
      showWatingAdd: true,
      adToken: token
    })
    let that = this
    let nowTime = 0
    let interval = setInterval(function () {
      if (time > nowTime) {
        nowTime += 1
        that.setData({
          adProgress: nowTime / time,
          lastAdTime: time - nowTime
        })
        console.log(that.data.adProgress)
      } else {
        that.setData({
          lastAdTime: '等待结束',
          
        })
        clearInterval(interval)
      }
    }, 1000)
  },

  playAfterAdd() {
    if (this.data.adProgress < 0.9) return
    this.setData({
      showWatingAdd: false
    })
    this.playChoosed(this.data.choosed)
  }
})